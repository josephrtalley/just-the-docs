---
layout: default
title: 2022-Q1
parent: Objectives and Key Results
---

# 2022-Q1 OKRs and KPIs

## Objective 1: *Framework*
*Increase throughput by establishing department structure and performance framework.*
<hr>

### `KR 1-1` Establish general technology handbook and publish. 

- [ ] Create mission/principles/objectives/metrics/org content.
- [ ] Establish protocol for publishing and version control.
- [ ] Publish live content to public site.

#### KPIs

*`key results completed (by quarter)`*

**target:** 12

**actual:** 

## Objective 2: *Customers*
*Improve customer experience by upgrading customer-centric applications.*
<hr>

### `KR 2-1` Establish requirements for Salesforce CRM and MuleSoft iPaaS. 

- [ ] Establish requirements for CRM Configuration.
- [ ] Establish requirements for Sales Cloud Configuration.
- [ ] Establish requirements for Service Cloud Configuration.
- [ ] Establish requirements for Community Configuration.
- [ ] Establish requirements for Dataroma Configuration.
- [ ] Establish requirements for Marketing Cloud Configuration.
- [ ] Establish requirements for MuleSoft Collaboration.
- [ ] Establish requirements for CRM Integrations.
- [ ] Establish requirements for Data Migration.
- [ ] Establish requirements for Reporting.
- [ ] Approve technical documents.

#### KPIs

*`net promoter score (TTM)`*

**target:** 9

**actual:** 

## Objective 3: *Accounting*
*Reduce manual journal entries and manual system inputs by introducing controls for invoices and automated consolidations for parent/child company relationships.*
<hr>

### `KR 3-1` Implement Concur for invoice management.

- [ ] Import vendors, expense types, custom fields.
- [ ] Setup approval workflows.
- [ ] Setup custom export template.
- [ ] Import users and update permissions.
- [ ] Create documentation and conduct training.
- [ ] Retire Wrike platform after transition period.

### `KR 3-2` Establish new architecture for JV accounting in Newstar.

- [ ] Establish project creation methodology and naming convention.
- [ ] Confirm object relationships and functionality with Constellation.
- [ ] Test consolidation module in Newstar proto environment.
- [ ] Create new project/division/company records for new JVs.
- [ ] Copy live database to proto database.
- [ ] Test opening balances and consolidations in Newstar proto.
- [ ] Record opening balances and consolidations in Newstar live.
- [ ] Troubleshoot reporting and integrated tools.

#### KPIs

*`???`*

**target:** ??

**actual:** 

## Objective 4: *Construction*
*Reduce closing delays and back-end punch items by establishing company-wide system playbooks related to quality and increasing visibility into unit-specific construction schedules.*
<hr>

### `KR 4-1` Create documentation for Procore quality and safety tools.

- [ ] Review inspections tool.
- [ ] Review observations tool.
- [ ] Review punch list tool.
- [ ] Review daily logs tool.
- [ ] Review incidents tool.
- [ ] Finalize documentation for all tools.
- [ ] Finalize application configuration for all tools.
- [ ] Present findings and final documentation.

### `KR 4-2` Integrate unit-specific milestone data from MS Project schedules.

- [ ] Establish connection to data from existing MS Project schedule.
- [ ] Parse task names for phase, unit, and task code fields.
- [ ] Transform and append data to Newstar Units table.
- [ ] Create documentation and conduct training.

#### KPIs

*`???`*

**target:** ??

**actual:** 

## Objective 5: *Enablement*
*...*
<hr>

### `KR 5-1` Upgrade Austin office audio/video infrastructure.

- [ ] Assess options and receive proposal from vendor.
- [ ] Confirm feasibility and plan installation methods.
- [ ] Procure/deploy/test at Tango conference room.
- [ ] Create documentation and conduct training.
- [ ] Procure/deploy/test at Mist conference room.

### `KR 5-2` Test all file and data backup systems.

- [ ] Office365 (Outlook/SharePoint)
- [ ] Network Attached Storage (design files)
- [ ] Azure VM (Newstar application files and all databases)
- [ ] Azure File Shares (accounting files)

### `KR 5-3` Remove unused application licenses.

- [ ] Remove unused Bluebeam licenses.
- [ ] Audit Adobe users and remove unused licenses.
- [ ] Audit Power BI users and remove unused licenses.
- [ ] Audit Azure VD users.
- [ ] Remove unused Office 365 licenses.

### `KR 5-4` Update tenant domain name.

- [ ] Research and document benefits, risks, and mitigation steps.
- [ ] Determine and create new domain name.
- [ ] Communicate benefit, risk, and mitigation to users in advance.
- [ ] Update domain name and conduct testing.

### `KR 5-6` Assess new hardware procurement and provisioning partners.

- [ ] Establish requirements.
- [ ] Investigate at least three vendor options.
- [ ] Provide imaging specifications to viable vendors.
- [ ] Make decision to keep existing process or move to new vendor.

#### KPIs

*`???`*

**target:** ??

**actual:** 

# Technology Metrics

**`technology spend (TTM)`** is the total invoiced amount for hardware, software, and third-party technology professional services over the trailing 12 months.

A moderately positive correlation between technology spend and other growth metrics indicates proper investment for future growth while gaining efficiencies due to economies of scale/scope.


_`technology spend as % of total spend (TTM)`_
_`technology spend as % of corporate spend (TTM)`_
_`technology spend per employee (TTM)`_
_`technology spend by department (TTM)`_
<hr>

**`technology key results completed (TTM)`**

*`technology key results completed (by quarter)`*
<hr>

**`technology user satisfaction score (TTM)`**
<hr>

**`helpdesk tickets per employee (TTM)`**
<hr>

**`Licensed self-service data users (pro)`**
<hr>

**`Monthly self-service data unique users (pro)`**
<hr>

**`Monthly self-service report views (pro)`**
<hr>

**`Monthly self-service report views (free)`**
<hr>

**`Microsoft Secure Score`**
<hr>

**`Microsoft Productivity Score`**
<hr>

**`Microsoft Compliance Score`**
<hr>

**`Phishing simulation compromised users`**
<hr>

**`Training/support talks per month`**
<hr>

**`Days since last audit (by application)`**
<hr>

**`% spend by type (invoice/credit card)`**
<hr>

# Shared Metrics

**`revenue`**
<hr>

**`profit`**
<hr>

**`total spend`**
<hr>

**`corporate spend`**
<hr>

**`average monthly employee headcount`**
<hr>

**`onboarding user satisfaction score`**
<hr>

**`under/over budget COS estimates`**

